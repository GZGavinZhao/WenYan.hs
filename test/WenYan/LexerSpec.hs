{-# LANGUAGE QuasiQuotes #-}

module WenYan.LexerSpec (spec) where

import Test.Hspec
import Text.RawString.QQ
import WenYan.Lexer

toTokens :: [RangedToken] -> [Token]
toTokens = map rtToken

fromRight' :: (HasCallStack, Show l) => Either l r -> r
fromRight' (Right right) = right
fromRight' (Left left) = error $ show left

scanToTokens :: HasCallStack => String -> [Token]
scanToTokens str = toTokens $ fromRight' $ scanMany str

spec :: Spec
spec = do
  describe "lexer" $ do
    it "riots when parsing nonsense" $ do
      scanMany "fdsfsd" `shouldBe` Left "lexical error at line 1, column 1"
      scanMany "114514" `shouldBe` Left "lexical error at line 1, column 1"

    it "ignores whitespace" $ do
      scanToTokens "   " `shouldBe` [EOF]

    it "treats 『。』 and 『、』 as whitespace" $ do
      scanToTokens "  、、 " `shouldBe` [EOF]
      scanToTokens " 。。 、、 " `shouldBe` [EOF]

    it "lexes characters between 『「』 and 『」』as identifiers" $ do
      scanToTokens "「甲」" `shouldBe` [Identifier "甲", EOF]
      scanToTokens "「甲乙」" `shouldBe` [Identifier "甲乙", EOF]
      scanMany "「」" `shouldBe` Left "lexical error at line 1, column 3"

    it "lexes characters between 『「「』 and 『」」』as identifiers" $ do
      scanToTokens "「「問天地好在」」" `shouldBe` [String "問天地好在", EOF]
      scanToTokens "「「」」" `shouldBe` [String "", EOF]

    it "lexes Hanzi integers" $
      let checkInt s i = scanToTokens s `shouldBe` [HanziInt i s, EOF]
       in ( do
              checkInt "零" 0
              checkInt "零" 0
              checkInt "一" 1
              checkInt "十" 10
              checkInt "百" 100
              checkInt "千" 1000
              checkInt "萬" 10000
              checkInt "億" 100000000
              checkInt "十三" 13
              checkInt "一百" 100
              checkInt "二百五十" 250
              checkInt "一千零一" 1001
              checkInt "十一萬四千五百一十四" 114514
              checkInt "三千七百萬" 37000000
              checkInt "三億" 300000000
              checkInt "三億零一" 300000001
              checkInt "三億零一十二" 300000012
          )

    it "lexes Hanzi floats" $ do
      scanToTokens "五又三分" `shouldBe` [HanziFloat 5 "五又三分", EOF]
      pendingWith "Parsing Hanzi floats is not implemented yet"

    it "lexes Hanzi bools" $ do
      scanToTokens "陽" `shouldBe` [Bool True, EOF]
      scanToTokens "陰" `shouldBe` [Bool False, EOF]

    it "lexes WenYan types" $ do
      scanToTokens "數" `shouldBe` [WYNum, EOF]
      scanToTokens "列" `shouldBe` [WYList, EOF]
      scanToTokens "言" `shouldBe` [WYString, EOF]
      scanToTokens "爻" `shouldBe` [WYBool, EOF]

    it "lexes WenYan logic binary operators" $ do
      scanToTokens "中有陽乎" `shouldBe` [IsTrue, EOF]
      scanToTokens "中無陰乎" `shouldBe` [IsFalse, EOF]
      scanToTokens "「甲」中有陽乎。" `shouldBe` [Identifier "甲", IsTrue, EOF]
      scanToTokens "變" `shouldBe` [Unary, EOF]

    it "lexes WenYan arithmetic binary operators" $ do
      scanToTokens "加" `shouldBe` [Plus, EOF]
      scanToTokens "減" `shouldBe` [Minus, EOF]
      scanToTokens "乘" `shouldBe` [Multiply, EOF]

    it "lexes post mod operator" $ do
      scanToTokens "所餘幾何" `shouldBe` [PostMod, EOF]

    it "lexes for loops ending" $ do
      scanToTokens "恆為是。乘「甲」以一。云云。" `shouldBe` [ForStartWhile, Multiply, Identifier "甲", PrepRight, HanziInt 1 "一", ForIfEnd, EOF]
      scanToTokens "恆為是。乘「甲」以一。也。" `shouldBe` [ForStartWhile, Multiply, Identifier "甲", PrepRight, HanziInt 1 "一", ForIfEnd, EOF]

    it "lexes comments" $ do
      scanToTokens "批曰「「文氣淋灕。字句切實」」。" `shouldBe` [Comment "文氣淋灕。字句切實", EOF]
      scanToTokens "注曰「「文氣淋灕。字句切實」」。" `shouldBe` [Comment "文氣淋灕。字句切實", EOF]
      scanToTokens "疏曰「「文氣淋灕。字句切實」」。" `shouldBe` [Comment "文氣淋灕。字句切實", EOF]

    it "lexes some pretty complex paragraphs of code" $ do
      scanToTokens
        [r|
吾有一數。曰三。名之曰「甲」。
為是「甲」遍。
	吾有一言。曰「「問天地好在。」」。書之。
云云。
|]
        `shouldBe` [WuYou, HanziInt 1 "一", WYNum, Yue, HanziInt 3 "三", MingZhi, Yue, Identifier "甲", ForStartEnum, Identifier "甲", ForMidEnum, WuYou, HanziInt 1 "一", WYString, Yue, String "問天地好在。", Print, ForIfEnd, EOF]
