module WenYan.Parser.ReturnSpecAns where

import WenYan.AST
import WenYan.Lexer (AlexPosn (AlexPn), Range (Range), start, stop)

emptyReturnAns :: Either String [Statement Range]
emptyReturnAns =
  Right
    [ ReturnStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 4 1 5
            }
        )
        Nothing
        False
    ]

returnWithIntAns :: Either String [Statement Range]
returnWithIntAns =
  Right
    [ ReturnStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 3 1 4
            }
        )
        ( Just
            ( EInt
                ( Range
                    { start = AlexPn 2 1 3,
                      stop = AlexPn 3 1 4
                    }
                )
                3
                "三"
            )
        )
        True
    ]

returnWithVarAns :: Either String [Statement Range]
returnWithVarAns =
  Right
    [ ReturnStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 5 1 6
            }
        )
        ( Just
            ( EVar
                ( Range
                    { start = AlexPn 2 1 3,
                      stop = AlexPn 5 1 6
                    }
                )
                "甲"
            )
        )
        True
    ]

returnWithQiAns :: Either String [Statement Range]
returnWithQiAns =
  Right
    [ ReturnStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 3 1 4
            }
        )
        ( Just
            ( Qi
                ( Range
                    { start = AlexPn 2 1 3,
                      stop = AlexPn 3 1 4
                    }
                )
            )
        )
        True
    ]

returnWithPrevAns :: Either String [Statement Range]
returnWithPrevAns =
  Right
    [ ReturnStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 3 1 4
            }
        )
        Nothing
        True
    ]
