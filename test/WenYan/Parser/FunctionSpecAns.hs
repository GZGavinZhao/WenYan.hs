module WenYan.Parser.FunctionSpecAns where

import WenYan.AST
import WenYan.Lexer (AlexPosn (AlexPn), Range (Range), start, stop)

simpleFunctionDefAns :: Either a [Statement Range]
simpleFunctionDefAns =
  Right
    [ FnDefineStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 44 1 45
            }
        )
        1
        ( Called
            ( Range
                { start = AlexPn 8 1 9,
                  stop = AlexPn 12 1 13
                }
            )
            "階乘"
        )
        [ ( 1,
            WYNum
              ( Range
                  { start = AlexPn 22 1 23,
                    stop = AlexPn 23 1 24
                  }
              ),
            [ Called
                ( Range
                    { start = AlexPn 25 1 26,
                      stop = AlexPn 28 1 29
                    }
                )
                "甲"
            ]
          )
        ]
        []
        ( EVar
            ( Range
                { start = AlexPn 37 1 38,
                  stop = AlexPn 41 1 42
                }
            )
            "階乘"
        )
    ]

preCallWithNoArgsAns :: Either String [Statement Range]
preCallWithNoArgsAns =
  Right
    [ FnPreCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 5 1 6
            }
        )
        ( EVar
            ( Range
                { start = AlexPn 1 1 2,
                  stop = AlexPn 5 1 6
                }
            )
            "翻倍"
        )
        []
        Nothing
    ]

preCallWithOneArgAns :: Either String [Statement Range]
preCallWithOneArgAns =
  Right
    [ FnPreCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 10 1 11
            }
        )
        ( EVar
            ( Range
                { start = AlexPn 1 1 2,
                  stop = AlexPn 5 1 6
                }
            )
            "翻倍"
        )
        [ ( LeftPreposition
              ( Range
                  { start = AlexPn 5 1 6,
                    stop = AlexPn 6 1 7
                  }
              ),
            EVar
              ( Range
                  { start = AlexPn 6 1 7,
                    stop = AlexPn 10 1 11
                  }
              )
              "大衍"
          )
        ]
        Nothing
    ]

preCallWithTwoArgsAns :: Either String [Statement Range]
preCallWithTwoArgsAns =
  Right
    [ FnPreCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 22 1 23
            }
        )
        ( EVar
            ( Range
                { start = AlexPn 1 1 2,
                  stop = AlexPn 7 1 8
                }
            )
            "吸星大法"
        )
        [ ( LeftPreposition
              ( Range
                  { start = AlexPn 7 1 8,
                    stop = AlexPn 8 1 9
                  }
              ),
            EVar
              ( Range
                  { start = AlexPn 8 1 9,
                    stop = AlexPn 14 1 15
                  }
              )
              "桃谷六仙"
          ),
          ( LeftPreposition
              ( Range
                  { start = AlexPn 15 1 16,
                    stop = AlexPn 16 1 17
                  }
              ),
            EVar
              ( Range
                  { start = AlexPn 16 1 17,
                    stop = AlexPn 22 1 23
                  }
              )
              "不戒和尚"
          )
        ]
        Nothing
    ]

preCallWithOneArgAndAssignAns :: Either String [Statement Range]
preCallWithOneArgAndAssignAns =
  Right
    [ FnPreCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 17 1 18
            }
        )
        ( EVar
            ( Range
                { start = AlexPn 1 1 2,
                  stop = AlexPn 5 1 6
                }
            )
            "翻倍"
        )
        [ ( LeftPreposition
              ( Range
                  { start = AlexPn 5 1 6,
                    stop = AlexPn 6 1 7
                  }
              ),
            EVar
              ( Range
                  { start = AlexPn 6 1 7,
                    stop = AlexPn 10 1 11
                  }
              )
              "大衍"
          )
        ]
        ( Just
            ( Called
                ( Range
                    { start = AlexPn 14 1 15,
                      stop = AlexPn 17 1 18
                    }
                )
                "甲"
            )
        )
    ]

postCallWithOneArgAns :: Either String [Statement Range]
postCallWithOneArgAns =
  Right
    [ FnPostCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 7 1 8
            }
        )
        [ ( 2,
            EVar
              ( Range
                  { start = AlexPn 4 1 5,
                    stop = AlexPn 7 1 8
                  }
              )
              "丁"
          )
        ]
        Nothing
    ]

postCallWithTwoArgsAns :: Either String [Statement Range]
postCallWithTwoArgsAns =
  Right
    [ FnPostCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 15 1 16
            }
        )
        [ ( 2,
            EVar
              ( Range
                  { start = AlexPn 4 1 5,
                    stop = AlexPn 7 1 8
                  }
              )
              "丁"
          ),
          ( 2,
            EVar
              ( Range
                  { start = AlexPn 12 1 13,
                    stop = AlexPn 15 1 16
                  }
              )
              "戊"
          )
        ]
        Nothing
    ]

postCallWithTwoArgsAndAssignAns :: Either String [Statement Range]
postCallWithTwoArgsAndAssignAns =
  Right
    [ FnPostCallStmt
        ( Range
            { start = AlexPn 0 1 1,
              stop = AlexPn 22 1 23
            }
        )
        [ ( 2,
            EVar
              ( Range
                  { start = AlexPn 4 1 5,
                    stop = AlexPn 7 1 8
                  }
              )
              "丁"
          ),
          ( 2,
            EVar
              ( Range
                  { start = AlexPn 12 1 13,
                    stop = AlexPn 15 1 16
                  }
              )
              "戊"
          )
        ]
        ( Just
            ( Called
                ( Range
                    { start = AlexPn 19 1 20,
                      stop = AlexPn 22 1 23
                    }
                )
                "己"
            )
        )
    ]
