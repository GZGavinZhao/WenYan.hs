{-# LANGUAGE ImportQualifiedPost #-}

module WenYan.Parser.FunctionSpec (spec) where

import Test.Hspec
import WenYan.AST
import WenYan.Parser
import WenYan.Parser.FunctionSpecAns
import WenYan.Lexer (Range)
import WenYan.Lexer qualified as L

parse :: String -> Either String [Statement Range]
parse s = L.runAlex s parseWenYan

spec :: Spec
spec = do
  describe "parser" $ do
    it "parses function declaration" $ do
      parse "吾有一術。名之曰「階乘」。欲行是術。必先得一數。曰「甲」。乃行是術曰。是謂「階乘」之術也。"
        `shouldBe` simpleFunctionDefAns

    it "parses function precall" $ do
      parse "施「翻倍」" `shouldBe` preCallWithNoArgsAns

      parse "施「翻倍」於「大衍」。"
        `shouldBe` preCallWithOneArgAns

      parse "施「吸星大法」於「桃谷六仙」、於「不戒和尚」。"
        `shouldBe` preCallWithTwoArgsAns

      parse "施「翻倍」於「大衍」。名之曰「甲」。"
        `shouldBe` preCallWithOneArgAndAssignAns

    it "parses function postcall" $ do
      parse "取二以施「丁」。" `shouldBe` postCallWithOneArgAns

      parse "取二以施「丁」。取二以施「戊」。"
        `shouldBe` postCallWithTwoArgsAns

      parse "取二以施「丁」。取二以施「戊」。名之曰「己」。"
        `shouldBe` postCallWithTwoArgsAndAssignAns
