module WenYan.Parser.LoopSpec (spec) where

import Test.Hspec
import WenYan.AST
import WenYan.Parser
import WenYan.Lexer (AlexPosn (AlexPn), Range (Range), start, stop)
import WenYan.Lexer qualified as L

parse :: String -> Either String [Statement Range]
parse s = L.runAlex s parseWenYan

spec :: Spec
spec = do
  describe "parser" $ do
    it "parses for-in style loops" $ do
      parse "凡「天地」中之「人」。云云。"
        `shouldBe` Right
          [ ForListStmt
              ( Range
                  { start = AlexPn 0 1 1,
                    stop = AlexPn 13 1 14
                  }
              )
              ( EVar
                  ( Range
                      { start = AlexPn 1 1 2,
                        stop = AlexPn 5 1 6
                      }
                  )
                  "天地"
              )
              ( EVar
                  ( Range
                      { start = AlexPn 7 1 8,
                        stop = AlexPn 10 1 11
                      }
                  )
                  "人"
              )
              []
          ]

    it "parses enum style loops" $ do
      parse "為是百遍。云云。"
        `shouldBe` Right
          [ ForEnumStmt
              ( Range
                  { start = AlexPn 0 1 1,
                    stop = AlexPn 7 1 8
                  }
              )
              ( EInt
                  ( Range
                      { start = AlexPn 2 1 3,
                        stop = AlexPn 3 1 4
                      }
                  )
                  100
                  "百"
              )
              []
          ]

    it "parses while loops" $ do
      parse "恆為是。云云。"
        `shouldBe` Right
          [ ForWhileStmt
              ( Range
                  { start = AlexPn 0 1 1,
                    stop = AlexPn 6 1 7
                  }
              )
              []
          ]
