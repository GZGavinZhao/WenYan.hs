module WenYan.Parser.ReturnSpec where

import Test.Hspec
import WenYan.AST
import WenYan.Parser
import WenYan.Parser.ReturnSpecAns
import WenYan.Lexer (Range)
import WenYan.Lexer qualified as L

parse :: String -> Either String [Statement Range]
parse s = L.runAlex s parseWenYan

spec :: Spec
spec = do
  describe "parser" $ do
    it "parses empty return" $ do
      parse "乃歸空無。" `shouldBe` emptyReturnAns

    it "parses return with expressions" $ do
      parse "乃得三。" `shouldBe` returnWithIntAns
      parse "乃得「甲」。" `shouldBe` returnWithVarAns

    it "parses return that returns the last expression" $ do
      parse "乃得其。" `shouldBe` returnWithQiAns
      parse "乃得矣。" `shouldBe` returnWithPrevAns
