{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Use newtype instead of data" #-}

module Main (main) where

import Control.Exception
import Control.Monad.State hiding (void)
import Data.ByteString.Char8 qualified as BS
import Data.Either
import Debug.Trace
import Development.Placeholders
import Foreign hiding (void)
import LLVM.AST (defaultModule, globalVariableDefaults, moduleDefinitions, moduleName)
import LLVM.AST qualified as AST
import LLVM.AST.Constant qualified as C
import LLVM.AST.Global (Global (..))
import LLVM.AST.Linkage (Linkage (..))
import LLVM.AST.Operand (Operand (..))
import LLVM.AST.Type as TY
import LLVM.Analysis
import LLVM.Context
import LLVM.IRBuilder
import LLVM.Module
import LLVM.OrcJIT
import LLVM.Target (withHostTargetMachineDefault)
import Text.Pretty.Simple
import Prelude hiding (lookup)

foreign import ccall "dynamic" haskFun :: FunPtr (IO ()) -> IO ()

run :: FunPtr a -> IO ()
run fn = haskFun (castFunPtr fn :: FunPtr (IO ()))

main :: IO ()
main =
  let -- fstState = runModuleBuilder emptyModuleBuilder buildMyModule
      myModule =
        defaultModule
          { moduleName = "testModule",
            moduleDefinitions = execModuleBuilder emptyModuleBuilder buildMyModule
            -- moduleDefinitions = snd fstState
          }

      my2ndModule =
        defaultModule
          { moduleName = "modifyModule",
            moduleDefinitions = execModuleBuilder emptyModuleBuilder buildModifyModule
          }

      buildModifyModule :: ModuleBuilder ()
      buildModifyModule = do
        hahaha <- externVar "hahaha" i32
        putchar <- extern "putchar" [TY.i32] TY.void
        _ <- function "wow" [] void $ \_ -> do
          _ <- block
          store hahaha 0 (int32 $ toInteger $ fromEnum 'x')
          htmp <- load hahaha 0
          _ <- call putchar [(htmp, [])]
          return ()
        return ()
   in do
        withContext $ \context -> do
          llvmModule <- createModuleFromAST context myModule
          verify llvmModule
          putStrLn . BS.unpack =<< moduleLLVMAssembly llvmModule

          modifyModule <- createModuleFromAST context my2ndModule
          verify modifyModule
          putStrLn . BS.unpack =<< moduleLLVMAssembly modifyModule

          withHostTargetMachineDefault $ \tm -> do
            withExecutionSession $ \es -> do
              objectLayer <- createRTDyldObjectLinkingLayer es
              irLayer <- createIRCompileLayer es objectLayer tm
              defaultLib <- createJITDylib es "main"
              addDynamicLibrarySearchGeneratorForCurrentProcess irLayer defaultLib

              let lookup = lookupSymbol es irLayer defaultLib
              let addMod m = slipr addModule defaultLib irLayer =<< cloneAsThreadSafeModule m

              addMod llvmModule
              addMod modifyModule

              print =<< lookup "hahaha"
              wow2 <- either (error . show) id <$> lookup "wow"

              run $ castPtrToFunPtr (wordPtrToPtr $ jitSymbolAddress wow2)

              return ()

-- -- withContext $ \context -> do
-- modModule <- createModuleFromAST context modifyModule
-- -- verify modModule
-- putStrLn . BS.unpack =<< moduleLLVMAssembly modModule

buildMyModule :: ModuleBuilder () -- Operand
buildMyModule = do
  hahaha <- global "hahaha" i32 (C.Int 32 0)
  putchar <- extern "putchar" [TY.i32] TY.void
  _ <- function "wow" [] void $ \_ -> do
    _ <- block
    store hahaha 0 (int32 $ toInteger $ fromEnum 'y')
    htmp <- load hahaha 0
    _ <- call putchar [(htmp, [])]
    return ()
  -- modify $ \s -> s {prevOperand = hahaha}
  -- return hahaha
  return ()

-- | An external variable definition
externVar ::
  MonadModuleBuilder m =>
  -- | Definition name
  AST.Name ->
  -- | Variable type
  Type ->
  m Operand
externVar nm ty = do
  emitDefn $
    AST.GlobalDefinition
      AST.globalVariableDefaults
        { name = nm,
          linkage = External,
          type' = ty
        }
  let ptrTy = ptr ty
  pure $ ConstantOperand $ C.GlobalReference ptrTy nm

slipr :: (a -> b -> c -> d) -> b -> c -> a -> d
slipr f b c a = f a b c
