{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module WenYan.Codegen (codegenModule) where

import Control.Exception (assert)
import Control.Monad.State hiding (void)
import Data.Map qualified as M
import Data.Maybe
import Debug.Trace
import Development.Placeholders
import LLVM.AST
import LLVM.AST.AddrSpace qualified
import LLVM.AST.Constant qualified as C
import LLVM.AST.Global (Global (..))
import LLVM.AST.Type
import LLVM.IRBuilder as IRB
import WenYan.AST

type InnerCodegen = (State InnerCodegenState)

-- This holds the state when generating a single top-level statement, most
-- commonly used on generating a function declaration
data InnerCodegenState = InnerCodegenState
  { variableTable :: M.Map Name (Type, Operand),
    functionTable :: M.Map Name (Type, Operand),
    nameSupply :: Word,
    prevVal :: Maybe Name
  }

initInnerCodegenState :: Word -> ModuleCodegenState -> InnerCodegenState
initInnerCodegenState ns mcs =
  InnerCodegenState
    { variableTable = declaredVariables mcs,
      functionTable = declaredFunctions mcs,
      nameSupply = ns,
      prevVal = Nothing
    }

data ModuleCodegenState = ModuleCodegenState
  { declaredVariables :: M.Map Name (Type, Operand),
    declaredFunctions :: M.Map Name (Type, Operand),
    mainFuncBlocks :: [BasicBlock]
  }

initModuleCodegenState :: ModuleCodegenState
initModuleCodegenState =
  ModuleCodegenState
    { declaredVariables = M.empty,
      declaredFunctions = M.empty,
      mainFuncBlocks = []
    }

type ModuleCodegen = State ModuleCodegenState

codegenModule :: [Statement a] -> Module
codegenModule stmts = evalState mainModule initModuleCodegenState
  where
    mainModule = buildModuleT "Main WenYan Module" builder
    builder = mapM_ codegenTopLevel stmts

codegenTopLevel :: Statement a -> ModuleBuilderT ModuleCodegen ()
codegenTopLevel (FnDefineStmt loc fnCnt (Called _ name) params body name2) = do
  let computeBlocks =
        execIRBuilderT
          emptyIRBuilder
          ( do
              _ <- block `named` "entry"

              forM_ (expandParams params) $ \(argType, argName) -> do
                arg <- alloca argType Nothing 0
                return ()

              retval <- last <$> mapM codegen body
              maybe retVoid ret retval
          )

  curModuleCodegenState <- get
  let blocks = evalState computeBlocks (initInnerCodegenState 0 curModuleCodegenState)

  emitDefn $
    GlobalDefinition
      functionDefaults
        { name = mkName name,
          basicBlocks = blocks
        }

  $notImplemented
codegenTopLevel _ = trace "Shouldn't be a top-level stuff" $ return ()

codegen :: Statement a -> IRBuilderT InnerCodegen (Maybe Operand)
codegen stmt = $notImplemented

convertTypes :: WYType a -> Type
convertTypes (WYNum _) = i32
convertTypes (WYBool _) = i1
convertTypes _ = $notImplemented

calledToName :: Called a -> Name
calledToName (Called _ name) = mkName name

expandParams :: [(Integer, WYType a, [Called a])] -> [(Type, Name)]
expandParams = concatMap expandSingle
  where
    expandSingle :: (Integer, WYType a, [Called a]) -> [(Type, Name)]
    expandSingle (cnt, ty, names) = assert (fromInteger cnt == length names) $ map ((convertTypes ty,) . calledToName) names
