{-# LANGUAGE TemplateHaskell #-}

module WenYan.IR (irGen) where

import Control.Applicative
import Control.Exception
import Control.Monad
import Control.Monad.Cont
import Control.Monad.Except
import Control.Monad.RWS
import Control.Monad.State
import Data.List
import Data.Map (Map (..))
import Data.Map qualified as M
import Data.Maybe
import Data.Sequence (Seq)
import Data.Set (Set (..))
import Data.Set qualified as S
import Development.Placeholders
import WenYan.AST qualified as AST
import WenYan.Lexer qualified as L

data Op
  = Add
  | Sub
  | Mult
  | Div
  | Mod
  | Equal
  | Neq
  | Less
  | Leq
  | Greater
  | Geq
  | And
  | Or
  deriving (Show, Eq)

data Uop
  = Neg
  | Not
  deriving (Show, Eq)

data Type = TyInt | TyFloat | TyBool | TyList | TyString | TyVoid
  deriving (Show, Eq)

-- TExpr is an Expr annotated with a type
type TExpr = (Type, Expr)

data Expr
  = Literal Int
  | Fliteral Double
  | StrLit String
  | BoolLit Bool
  | BinOp Op TExpr TExpr
  | UnOp Uop TExpr
  | LVal LValue
  | Call String [TExpr]
  | Assign LValue TExpr
  | Noexpr
  deriving (Show, Eq)

-- | LValues are the class of assignable expressions that can appear
-- on the Left side on the '=' operator and that can have their addresses
-- taken.
data LValue = Access LValue Int | Id String
  deriving (Show, Eq)

data Bind = Bind {bindType :: Type, bindName :: String} deriving (Show, Eq)

bindToTExpr :: Bind -> TExpr
bindToTExpr bind = (bindType bind, LVal (Id $ bindName bind))

data Statement
  = TExpr TExpr
  | Block [Statement]
  | Return TExpr
  | If TExpr Statement Statement
  | While TExpr Statement
  | Break
  | Continue
  deriving (Show, Eq)

data Function = Function
  { returnTy :: Type,
    name :: String,
    args :: [Bind],
    body :: [Statement]
  }

type Program = ([Bind], [Function], [Statement])

-- Utils

data IRGenState = IRGenState
  { globalBinds :: Map String Bind,
    globalFuncs :: Map String Function,
    -- Locally declared variables and functions must have their names in the
    -- order they're declared, so we use a list.
    localBinds :: [Bind],
    localFuncs :: [Function],
    mainStmts :: [Statement],
    stack :: [TExpr],
    -- A seed that is incremented to use in all sorts of variable names.
    -- If it overflows, well, shame on you :P
    seed :: Int
  }

emptyIRGenState :: IRGenState
emptyIRGenState =
  IRGenState
    { globalBinds = M.empty,
      globalFuncs = M.empty,
      localBinds = [],
      localFuncs = [],
      mainStmts = [],
      stack = [],
      seed = 0
    }

genNextLevelState :: IRGen IRGenState
genNextLevelState = do
  IRGenState
    { globalBinds = existingBinds,
      localBinds = currentBinds,
      globalFuncs = existingFuncs,
      localFuncs = currentFuncs,
      seed = currentSeed
    } <-
    get

  return $
    IRGenState
      { globalBinds = M.union existingBinds (M.fromList $ zip (map bindName currentBinds) currentBinds),
        globalFuncs = M.union existingFuncs (M.fromList $ zip (map name currentFuncs) currentFuncs),
        localBinds = [],
        localFuncs = [],
        mainStmts = [],
        stack = [],
        seed = currentSeed + 1
      }

type IRGen = State IRGenState

addVar :: String -> Type -> IRGen ()
addVar name ty = modify $ \state -> state {localBinds = localBinds state ++ [Bind {bindType = ty, bindName = name}]}

genWhileLoopVar :: IRGen String
genWhileLoopVar = do
  curSeed <- gets seed
  return $ "wy_while_idx_" ++ show curSeed

clearStack :: IRGen ()
clearStack = modify $ \state -> state {stack = []}

addStmt :: Statement -> IRGen ()
addStmt stmt = modify $ \state -> state {mainStmts = mainStmts state ++ [stmt]}

swapStack :: TExpr -> IRGen ()
swapStack texpr = modify $ \state -> state {stack = [texpr]}

pushStack :: TExpr -> State IRGenState ()
pushStack texpr = do
  modify $ \state -> state {stack = texpr : stack state}

popStack :: State IRGenState TExpr
popStack = do
  top <- gets (head . stack)
  modify $ \state -> state {stack = tail (stack state)}
  return top

popEntireStack :: IRGen [TExpr]
popEntireStack = do
  top <- gets stack
  clearStack
  return top

getVar :: String -> [Bind] -> Bind
getVar name binds = fromJust $ find (\bind -> bindName bind == name) binds

convType :: AST.WYType a -> Type
convType (AST.WYNum _) = TyInt
convType (AST.WYList _) = TyList
convType (AST.WYString _) = TyString
convType (AST.WYBool _) = TyBool

convExpr :: AST.Exp a -> State IRGenState TExpr
convExpr (AST.EInt _ val _) = pure (TyInt, Literal $ fromInteger val)
convExpr (AST.EFloat _ val _) = $(todo "Floating point is not supported")
convExpr (AST.EString _ val) = $(todo "Built-in strings are not supported")
convExpr (AST.EBool _ val) = pure (TyBool, BoolLit val)
convExpr (AST.Qi _) = do
  top <- popStack
  clearStack
  return top
convExpr (AST.EVar _ name) = gets (bindToTExpr . getVar name . localBinds)

-- Generate

irGen :: [AST.Statement L.Range] -> Program
irGen stmts = (localBinds finalState, localFuncs finalState, mainStmts finalState)
  where
    finalState = execState (mapM_ gen stmts) emptyIRGenState

gen :: AST.Statement L.Range -> State IRGenState ()
-- 吾有一數。曰三。
gen (AST.DeclareStmt loc cnt ty' vals') = assert (length vals' == fromInteger cnt) $ do
  vals <- forM vals' convExpr
  forM_ vals $ \val -> do
    pushStack val
-- 吾有一數。曰三。名之曰「甲」。
gen (AST.DeclareDefineStmt loc cnt ty' vals' names') = assert (length vals' == fromInteger cnt) $ do
  vals <- forM vals' convExpr
  let names = map AST.extractCalled names'
  forM_ (zip names vals) $ \(name, val) -> do
    addStmt $ TExpr (TyVoid, Assign (Id name) val)
    pushStack val

-- 有數三。名之曰「甲」。
gen (AST.InitDefineStmt _ ty' val' Nothing) = do
  val <- convExpr val'
  assert (fst val == convType ty') pushStack val

-- 書之。
gen (AST.PrintStmt _) = do
  stack <- reverse <$> popEntireStack
  addStmt $ TExpr (TyVoid, Call "wyPrint" stack)

-- 凡「天地」中之「人」。⋯⋯ 云云。
gen (AST.ForListStmt _ list item body) = $(todo "for each loop not implemented yet")
-- 為是百遍。⋯⋯ 云云。
gen (AST.ForEnumStmt _ enum' stmts) = do
  loopVarName <- genWhileLoopVar
  enum <- convExpr enum'

  let loopVarBind = Bind {bindType = TyInt, bindName = loopVarName}
      loopVarLVal = Id loopVarName
      loopVarTExpr = (TyInt, LVal loopVarLVal)

  nextState <- genNextLevelState

  let finalState = execState (mapM_ gen stmts) nextState

  addStmt $
    Block
      [ TExpr (TyVoid, Assign loopVarLVal (TyInt, Literal 0)),
        While (TyBool, BinOp Less loopVarTExpr enum) $
          Block $
            mainStmts finalState
              ++ [TExpr (TyVoid, Assign loopVarLVal (TyInt, BinOp Add (TyInt, Literal 1) loopVarTExpr))]
      ]

  modify $ \state -> state {seed = seed finalState + 1}

errorAt loc msg = error $ show loc ++ ": " ++ msg
