{
{-# LANGUAGE ImportQualifiedPost #-}

module WenYan.Parser where

import Control.Applicative
import Control.Exception
import Data.Functor
import Data.Monoid
import Data.Maybe

import WenYan.Lexer qualified as L
import WenYan.AST
import WenYan.Utils
}

%name parseWenYan
%tokentype { L.RangedToken }
%error { parseError }
%monad { L.Alex } { >>= } { pure }
%lexer { lexer } { L.RangedToken L.EOF _ }

%token
    IDENTIFIER                         { L.RangedToken (L.Identifier _) _ }

    STRING_LITERAL                     { L.RangedToken (L.String _) _ }
    INT_NUM                            { L.RangedToken (L.HanziInt _ _) _ }
    FLOAT_NUM                          { L.RangedToken (L.HanziFloat _ _) _ }
    BOOL_VALUE                         { L.RangedToken (L.Bool _) _ }

    WYNUM                              { L.RangedToken L.WYNum _ }
    WYLIST                             { L.RangedToken L.WYList _ }
    WYSTRING                           { L.RangedToken L.WYString _ }
    WYBOOL                             { L.RangedToken L.WYBool _ }

    PLUS                               { L.RangedToken L.Plus _ }
    MINUS                              { L.RangedToken L.Minus _ }
    MULTIPLY                           { L.RangedToken L.Multiply _ }
    POST_MOD_MATH_OP                   { L.RangedToken L.PostMod _ }

    ISTRUE                             { L.RangedToken L.IsTrue _ }
    ISFALSE                            { L.RangedToken L.IsFalse _ }
    UNARY_OP                           { L.RangedToken L.Unary _ }

    PREPOSITION_LEFT                   { L.RangedToken L.PrepLeft _ }
    PREPOSITION_RIGHT                  { L.RangedToken L.PrepRight _ }

    IF                                 { L.RangedToken L.If _ }
    IFELSE                             { L.RangedToken L.Else _ }

    EQ                                 { L.RangedToken L.Eq _ }
    NEQ                                { L.RangedToken L.Neq _ }
    LE                                 { L.RangedToken L.Le _ }
    GE                                 { L.RangedToken L.Ge _ }
    GT                                 { L.RangedToken L.Gt _ }
    LT                                 { L.RangedToken L.Lt _ }

    FOR_START_LIST                     { L.RangedToken L.ForStartList _ }
    FOR_START_ENUM                     { L.RangedToken L.ForStartEnum _ }
    FOR_START_WHILE                    { L.RangedToken L.ForStartWhile _ }
    FOR_MID_LIST                       { L.RangedToken L.ForMidList _ }
    FOR_MID_ENUM                       { L.RangedToken L.ForMidEnum _ }
    FOR_IF_END                         { L.RangedToken L.ForIfEnd _ }

    PRINT                              { L.RangedToken L.Print _ }
    COMMENT                            { L.RangedToken (L.Comment _) _ }
    FLUSH                              { L.RangedToken L.Flush _ }
    BREAK                              { L.RangedToken L.Break _ }

    '夫'                               { L.RangedToken L.Fu _ }
    '之'                               { L.RangedToken L.Zhi _ }
    '其餘'                             { L.RangedToken L.QiYu _ }
    '長'                               { L.RangedToken L.Chang _ }
    '銜'                               { L.RangedToken L.Xian _ }
    '其'                               { L.RangedToken L.Qi _ }
    '充'                               { L.RangedToken L.Chong _ }
    '施'                               { L.RangedToken L.Shi _ }
    '取'                               { L.RangedToken L.Qu _ }
    '以施'                             { L.RangedToken L.YiShi _ }
    '吾有'                             { L.RangedToken L.WuYou _ }
    '術'                               { L.RangedToken L.Shu _ }
    '欲行是術'                         { L.RangedToken L.YuXingShiShu _ }
    '必先得'                           { L.RangedToken L.BiXianDe _ }
    '曰'                               { L.RangedToken L.Yue _ }
    '是術曰'                           { L.RangedToken L.ShiShuYue _ }
    '乃行是術曰'                       { L.RangedToken L.NaiHangShiShuYue _ }
    '是謂'                             { L.RangedToken L.ShiWei _ }
    '之術也'                           { L.RangedToken L.ZhiShuYe _ }
    '者'                               { L.RangedToken L.Zhe _ }
    '今有'                             { L.RangedToken L.JinYou _ }
    '名之'                             { L.RangedToken L.MingZhi _ }
    '有'                               { L.RangedToken L.You _ }
    '除'                               { L.RangedToken L.Chu _ }
    '昔之'                             { L.RangedToken L.XiZhi _ }
    '今'                               { L.RangedToken L.Jin _ }
    '是矣'                             { L.RangedToken L.ShiYi _ }
    '今不復存矣'                       { L.RangedToken L.JinBuFuCunYi _ }
    '乃得'                             { L.RangedToken L.NaiDe _ }
    '乃歸空無'                         { L.RangedToken L.NaiGuiKongWu _ }
    '乃得矣'                           { L.RangedToken L.NaiDeYi _ }
    '吾嘗觀'                           { L.RangedToken L.WuDangGuan _ }
    '之書'                             { L.RangedToken L.ZhiShu _ }
    '方悟'                             { L.RangedToken L.FangWu _ }
    '之義'                             { L.RangedToken L.ZhiYi _ }
    '物'                               { L.RangedToken L.Wu _ }
    '其物如是'                         { L.RangedToken L.QiWuRuShi _ }
    '物之'                             { L.RangedToken L.WuZhi _ }
    '之物也'                           { L.RangedToken L.ZhiWuYe _ }
%%



stmts :: { [Statement L.Range] }
    : list(stmt) { $1 }

stmt :: { Statement L.Range }
    : declare_statement  { $1 }
    | define_statement   { $1 }
    | print_statement    { $1 }
    | for_statement      { $1 }
    | function_statement { $1 }
    | return_statement   { $1 }
    | math_statement     { $1 }
    | assign_statement   { $1 }
    | import_statement   { $1 }



---------- Utilities ----------



-- Equivalent to p?
opt(p)
    :   { Nothing }
    | p { Just $1 }

rev_list1(p)
    : p              { [$1] }
    | rev_list1(p) p { $2 : $1 }

-- Equivalent to p+
list1(p) : rev_list1(p) { reverse $1 }
-- Equivalent to p*
list(p)
    : list1(p) { $1 }
    |          { [] }

fst(p,q)             : p q   { $1 }
snd(p,q)             : p q   { $2 }
trd(p,q,h)           : p q h { $3 }
both(p,q)            : p q   { ($1,$2) }
all3(p,q,h)          : p q h { ($1,$2,$3) }
mid3(p,q,h)          : p q h { $2 }
either(p, q)
    : p { $1 }
    | q { $1 }
either3(a,b,c)
    : a { $1 }
    | b { $1 }
    | c { $1 }
either4(a,b,c,d)
    : a { $1 }
    | b { $1 }
    | c { $1 }
    | d { $1 }

all4(a,b,c,d)        : a b c d { ($1, $2, $3, $4) }



---------- Actual Syntax ----------



called :: { Called L.Range }
    : '曰' IDENTIFIER { unTokToCalled $2 }

name_single :: { Called L.Range }
    : '名之' '曰' IDENTIFIER { unTokToCalled $3 }

name_multi :: { [Called L.Range] }
    : '名之' list1(snd('曰', IDENTIFIER)) { map unTokToCalled $2 }

type :: { WYType L.Range }
    : WYNUM    { WYNum (L.rtRange $1) }
    | WYLIST   { WYList (L.rtRange $1) }
    | WYSTRING { WYString (L.rtRange $1) }
    | WYBOOL   { WYBool (L.rtRange $1) }

int :: { Exp L.Range }
    : INT_NUM { unTok $1 (\range (L.HanziInt val str) -> EInt range val str) }

int_value :: { Integer }
    : INT_NUM { (\(L.RangedToken (L.HanziInt val _) _) -> val) $1 }

float :: { Exp L.Range }
    : FLOAT_NUM { unTok $1 (\range (L.HanziFloat val str) -> EFloat range val str) }

string :: { Exp L.Range }
    : STRING_LITERAL { unTok $1 (\range (L.String str) -> EString range str) }

var :: { Exp L.Range }
    : IDENTIFIER { unTok $1 (\range (L.Identifier name) -> EVar range name) }

qi  :: { Exp L.Range }
    : '其' { unTok $1 (\range L.Qi -> Qi range) }

bool :: { Exp L.Range }
    : BOOL_VALUE { unTok $1 (\range (L.Bool bool) -> EBool range bool) }

exp :: { Exp L.Range }
    : int    { $1 }
    | float  { $1 }
    | string { $1 }
    | var    { $1 }
    | bool   { $1 }

preposition :: { Preposition L.Range }
    : PREPOSITION_LEFT  { unTok $1 (\range L.PrepLeft -> LeftPreposition range) }
    | PREPOSITION_RIGHT { unTok $1 (\range L.PrepRight -> RightPreposition range) }


declare_statement :: { Statement L.Range }
    : '吾有' int_value type list1(snd('曰', exp)) { DeclareStmt (L.rtRange $1 <-> info (last $4)) $2 $3 $4 }


define_statement :: { Statement L.Range }
    : declare_statement name_multi 
        { (\(DeclareStmt range cnt wtype vals) -> DeclareDefineStmt (range <-> info (last $2)) cnt wtype vals $2) $1 }
    | init_define_statement { $1 }

init_define_statement :: { Statement L.Range }
    -- TODO(prettify): this looks ugly, but it works...
    -- I was hoping I could do something like `fromMaybe (info $3) (info <$> $4)
    : '有' type exp opt(name_single) { InitDefineStmt (L.rtRange $1 <-> if isNothing $4 then info $3 else info (fromJust $4)) $2 $3 $4 }


print_statement :: { Statement L.Range }
    : PRINT { unTok $1 (\range _ -> PrintStmt range) }


for_statement :: { Statement L.Range }
    : for_arr_statement   { $1 }
    | for_enum_statement  { $1 }
    | for_while_statement { $1 }

for_arr_statement :: { Statement L.Range }
    : FOR_START_LIST var FOR_MID_LIST var stmts FOR_IF_END
        { ForListStmt (L.rtRange $1 <-> L.rtRange $6) $2 $4 $5 }

for_enum_statement :: { Statement L.Range }
    : FOR_START_ENUM either(int, var) FOR_MID_ENUM stmts FOR_IF_END
        { ForEnumStmt (L.rtRange $1 <-> L.rtRange $5) $2 $4 }

for_while_statement :: { Statement L.Range }
    : FOR_START_WHILE stmts FOR_IF_END
        { ForWhileStmt (L.rtRange $1 <-> L.rtRange $3) $2 }


function_statement :: { Statement L.Range }
    : function_define_statement { $1 }
    | function_call_statement   { $1 }

function_define_statement :: { Statement L.Range }
    : '吾有' int_value '術' name_single opt(trd('欲行是術', '必先得', list1(all3(int_value, type, list1(called))))) either('是術曰', '乃行是術曰') stmts '是謂' var '之術也'
        { FnDefineStmt (L.rtRange $1 <-> L.rtRange $10) $2 $4 (fromMaybe [] $5) $7 $9 }

function_call_statement :: { Statement L.Range }
    -- Precall
    : '施' either(qi, var) list(both(preposition, exp)) opt(name_single)
        { FnPreCallStmt (L.rtRange $1 <-> maybeToInfo (if null $3 then $2 else (snd . last) $3) info $4) $2 $3 $4 }
    -- Postcall
    -- TODO: shift/reduce conflict!
    | list1(all4('取', int_value, '以施', var)) opt(name_single)
        { FnPostCallStmt ((L.rtRange . fstOf4 . head) $1 <-> maybeToInfo ((lstOf4 . last) $1) info $2) (map evenOf4 $1) $2 }


return_statement :: { Statement L.Range }
    : '乃得' either(exp, qi) { ReturnStmt (L.rtRange $1 <-> info $2) (Just $2) True }
    | '乃歸空無'             { ReturnStmt (L.rtRange $1) Nothing False }
    | '乃得矣'               { ReturnStmt (L.rtRange $1) Nothing True }


math_statement :: { Statement L.Range }
    : arith_binary_math_statement { $1 }
    | arith_unary_math_statement  { $1 }
    | boolean_algebra_statement   { $1 }
    | mod_math_statement          { $1 }

arith_binary_op :: { ArithBinaryOp L.Range }
    : PLUS     { Plus $ L.rtRange $1 }
    | MINUS    { Minus $ L.rtRange $1 }
    | MULTIPLY { Multiply $ L.rtRange $1 }

arith_unary_op :: { ArithUnaryOp L.Range }
    : UNARY_OP { ArithUnaryOp $ L.rtRange $1 }

arith_binary_math_statement :: { Statement L.Range }
    : arith_binary_op either(exp, qi) preposition either(exp, qi) opt(name_single)
        { ArithBinaryStmt (info $1 <-> maybe (info $4) info $5) $1 $2 $3 $4 $5 }

arith_unary_math_statement :: { Statement L.Range }
    : arith_unary_op either(exp, qi) opt(name_single)
        { ArithUnaryStmt (info $1 <-> maybe (info $2) info $3) $1 $2 $3 }

logic_binary_op :: { LogicBinaryOp L.Range }
    : ISTRUE  { IsTrue $ L.rtRange $1 }
    | ISFALSE { IsFalse $ L.rtRange $1 }

boolean_algebra_statement :: { Statement L.Range }
    : '夫' var var logic_binary_op opt(name_single)
        { BooleanAlgStmt (L.rtRange $1 <-> maybe (info $4) info $5) $2 $3 $4 $5 }

post_mod_math_op :: { PostModMathOp L.Range }
    : POST_MOD_MATH_OP { PostModMathOp $ L.rtRange $1 }

mod_math_statement :: { Statement L.Range }
    : '除' either4(int, float, var, qi) preposition either3(int, float, var) opt(post_mod_math_op)
        { ModMathStmt (L.rtRange $1 <-> maybe (info $4) info $5) $2 $3 $4 $5 }


assign_opt_field :: { Maybe (Exp L.Range) }
    : opt(snd('之', either3(int, string, var))) { $1 }

assign_statement :: { Statement L.Range }
    : '昔之' var '者' assign_opt_field '者' '今' either(exp, qi) '是矣'
        { AssignDataStmt (L.rtRange $1 <-> L.rtRange $8) $2 $4 $7 }
    | '昔之' var '者' assign_opt_field '者' '今' exp '之' int_value '是矣'
        { AssignArrayElmStmt (L.rtRange $1 <-> L.rtRange $10) $2 $4 $7 $9 }
    | '昔之' var '者' assign_opt_field '者' '今不復存矣'
        { AssignDestroyStmt (L.rtRange $1 <-> L.rtRange $6) $2 $4 }


import_statement :: { Statement L.Range }
    : '吾嘗觀' string '之書' opt(all3('方悟', list1(var), '之義'))
        { ImportStmt (L.rtRange $1 <-> (fromMaybe (L.rtRange $3) $ fmap (L.rtRange . lstOf3) $4)) $2 (fromMaybe [] $ fmap sndOf3 $4) }



---------- End ----------

{

parseError :: L.RangedToken -> L.Alex a
parseError _ = do
  (L.AlexPn _ line column, _, _, _) <- L.alexGetInput
  L.alexError $ "Parse error at line " <> show line <> ", column " <> show column

lexer :: (L.RangedToken -> L.Alex a) -> L.Alex a
lexer = (=<< L.alexMonadScan)

-- | Build a simple node by extracting its token type and range.
unTok :: L.RangedToken -> (L.Range -> L.Token -> a) -> a
unTok (L.RangedToken tok range) ctor = ctor range tok

-- | Unsafely extracts the the metainformation field of a node.
info :: Foldable f => f a -> a
info = fromJust . getFirst . foldMap pure

-- Invariant: the fallback token (x) must start before the maybe token (y)
maybeToInfo :: Foldable f => f L.Range -> (b -> L.Range) -> Maybe b -> L.Range
maybeToInfo x toInfo (Just y) = (info x) <-> (toInfo y)
maybeToInfo x _ Nothing = info x

sndOf3 (_, b, _) = b
lstOf3 (_, _, c) = c

-- | Performs the union of two ranges by creating a new range starting at the
-- start position of the first range, and stopping at the stop position of the
-- second range.
-- Invariant: The LHS range starts before the RHS range.
(<->) :: L.Range -> L.Range -> L.Range
L.Range a1 _ <-> L.Range _ b2 = assert (a1 < b2) $ L.Range a1 b2

unTokToCalled :: L.RangedToken -> Called L.Range
unTokToCalled token = unTok token (\range (L.Identifier name) -> Called range name)

}

-- vim: tabstop=4 shiftwidth=4 expandtab
