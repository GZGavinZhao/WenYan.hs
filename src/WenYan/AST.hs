{-# LANGUAGE GADTs #-}

module WenYan.AST where

--- 曰
data Called a where
  Called :: a -> String -> Called a
  deriving (Foldable, Show, Eq)

extractCalled :: Called a -> String
extractCalled (Called _ name) = name

data Preposition a where
  LeftPreposition :: a -> Preposition a
  RightPreposition :: a -> Preposition a
  deriving (Foldable, Show, Eq)

data ArithBinaryOp a where
  Plus :: a -> ArithBinaryOp a
  Minus :: a -> ArithBinaryOp a
  Multiply :: a -> ArithBinaryOp a
  deriving (Foldable, Show, Eq)

data ArithUnaryOp a where
  ArithUnaryOp :: a -> ArithUnaryOp a
  deriving (Foldable, Show, Eq)

data LogicBinaryOp a where
  IsTrue :: a -> LogicBinaryOp a
  IsFalse :: a -> LogicBinaryOp a
  deriving (Foldable, Show, Eq)

data PostModMathOp a where
  PostModMathOp :: a -> PostModMathOp a
  deriving (Foldable, Show, Eq)

--- Corresponds to `data` in the WenYan spec.
data Exp a where
  EInt :: a -> Integer -> String -> Exp a
  EFloat :: a -> Double -> String -> Exp a
  EList :: a -> String -> Exp a
  EVar :: a -> String -> Exp a
  EString :: a -> String -> Exp a
  EBool :: a -> Bool -> Exp a
  Qi :: a -> Exp a
  deriving (Foldable, Show, Eq)

extractVar :: Exp a -> String
extractVar (EVar _ name) = name
extractVar _ = error "Cannot extract variable name for a non-var Exp!"

data WYType a where
  WYNum :: a -> WYType a
  WYList :: a -> WYType a
  WYString :: a -> WYType a
  WYBool :: a -> WYType a
  deriving (Foldable, Show, Eq)

data Statement a where
  DeclareStmt :: a -> Integer -> (WYType a) -> [Exp a] -> Statement a
  DeclareDefineStmt ::
    a ->
    Integer ->
    (WYType a) ->
    [Exp a] ->
    [Called a] ->
    Statement a
  InitDefineStmt ::
    a ->
    (WYType a) ->
    (Exp a) ->
    (Maybe (Called a)) ->
    Statement a
  PrintStmt :: a -> Statement a
  ForListStmt ::
    a ->
    (Exp a) ->
    (Exp a) ->
    [Statement a] ->
    Statement a
  ForEnumStmt :: a -> (Exp a) -> [Statement a] -> Statement a
  ForWhileStmt :: a -> [Statement a] -> Statement a
  FnDefineStmt ::
    a ->
    Integer ->
    (Called a) ->
    [(Integer, WYType a, [Called a])] ->
    [Statement a] ->
    (Exp a) ->
    Statement a
  FnPreCallStmt ::
    a ->
    (Exp a) ->
    [(Preposition a, Exp a)] ->
    Maybe (Called a) ->
    Statement a
  FnPostCallStmt ::
    a ->
    [(Integer, Exp a)] ->
    Maybe (Called a) ->
    Statement a
  ReturnStmt ::
    a ->
    Maybe (Exp a) ->
    -- '乃得矣' is equivalent to '乃得其', but will have Nothing.
    -- This is just to remind that we want the previous evaluated expression.
    Bool ->
    Statement a
  ArithBinaryStmt ::
    a ->
    ArithBinaryOp a ->
    Exp a ->
    Preposition a ->
    Exp a ->
    Maybe (Called a) ->
    Statement a
  ArithUnaryStmt ::
    a ->
    ArithUnaryOp a ->
    Exp a ->
    Maybe (Called a) ->
    Statement a
  BooleanAlgStmt ::
    a ->
    Exp a ->
    Exp a ->
    LogicBinaryOp a ->
    Maybe (Called a) ->
    Statement a
  ModMathStmt ::
    a ->
    Exp a ->
    Preposition a ->
    Exp a ->
    Maybe (PostModMathOp a) ->
    Statement a
  AssignDataStmt ::
    a ->
    Exp a ->
    Maybe (Exp a) -> -- 之……者
    Exp a ->
    Statement a
  AssignArrayElmStmt ::
    a ->
    Exp a ->
    Maybe (Exp a) -> -- 之……者
    Exp a ->
    Integer -> -- int
    Statement a
  AssignDestroyStmt ::
    a ->
    Exp a ->
    Maybe (Exp a) -> -- 之……者
    Statement a
  ImportStmt ::
    a ->
    Exp a -> -- 模块之名
    [Exp a] ->
    Statement a
  deriving (Foldable, Show, Eq)
