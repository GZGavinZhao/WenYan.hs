{
module WenYan.Lexer
  ( -- * Invoking Alex
    Alex
  , AlexPosn (..)
  , alexGetInput
  , alexError
  , runAlex
  , alexMonadScan

  , Range (..)
  , RangedToken (..)
  , Token (..)
  , scanMany
  ) where

import WenYan.Utils
import Data.List
import Data.Maybe

}

-- TODO: I really want to use ByteStrings here, but they don't handle Chinese
-- characters (perhaps due to non ascii) well...
%wrapper "monadUserState"

%encoding "utf-8"

$int_num_keywords = [零一二三四五六七八九十百千萬億兆京垓秭穣溝澗正載極]
$float_num_keywords = [分釐毫絲忽微塵埃渺漠]
$type = [數列言爻]

@int_num = $int_num_keywords+
@string_literal = 「「([^\s\「]*)」」

tokens :-
    $white+                                     ;
    ([ \t\r\n'。''、'])+                        ;

    -- Identifiers
    「([^\s\「]+)」                             { tokId }

    -- Constants
    @string_literal                             { tokString }
    @int_num                                    { tokHanziInt }
    @int_num又(@int_num$float_num_keywords)+    { tokHanziFloat }
    [陰陽]                                      { tokBool }

    -- WenYan variable types
    數                                          { tok WYNum }
    列                                          { tok WYList }
    言                                          { tok WYString }
    爻                                          { tok WYBool }

    -- Arithmetic binary operators
    加                                          { tok Plus }
    減                                          { tok Minus }
    乘                                          { tok Multiply }
    -- Special case, mod (which is a post operator)
    所餘幾何                                    { tok PostMod }

    -- Logic binary operators
    中有陽乎                                    { tok IsTrue }
    中無陰乎                                    { tok IsFalse }
    變                                          { tok Unary }

    -- Prepositions
    於                                          { tok PrepLeft }
    以                                          { tok PrepRight }

    -- If/Else
    若                                          { tok If }
    若非                                        { tok Else }

    -- Comparison
    等於                                        { tok Eq } 
    不等於                                      { tok Neq } 
    不大於                                      { tok Le } 
    不小於                                      { tok Ge } 
    大於                                        { tok Gt }
    小於                                        { tok Lt }

    -- For loops
    凡                                          { tok ForStartList }
    為是                                        { tok ForStartEnum }
    恆為是                                      { tok ForStartWhile }
    中之                                        { tok ForMidList }
    遍                                          { tok ForMidEnum }
    云云|也                                     { tok ForIfEnd }

    -- Misc
    書之                                        { tok Print }
    (注曰|疏曰|批曰)@string_literal             { tokComment }
    噫                                          { tok Flush }
    乃止                                        { tok Break }

    -- The rest of all the crazy keywords.
    -- If you know a way to make it shorter/simpler/less verbose, so that I
    -- don't have to write in PinYin, please tell me!
    夫                                          { tok Fu }
    之                                          { tok Zhi }
    其餘                                        { tok QiYu }
    長                                          { tok Chang }
    銜                                          { tok Xian }
    其                                          { tok Qi }
    充                                          { tok Chong }
    施                                          { tok Shi }
    取                                          { tok Qu }
    以施                                        { tok YiShi }
    吾有                                        { tok WuYou }
    術                                          { tok Shu }
    欲行是術                                    { tok YuXingShiShu }
    必先得                                      { tok BiXianDe }
    曰                                          { tok Yue }
    是術曰                                      { tok ShiShuYue }
    乃行是術曰                                  { tok NaiHangShiShuYue }
    是謂                                        { tok ShiWei }
    之術也                                      { tok ZhiShuYe }
    者                                          { tok Zhe }
    今有                                        { tok JinYou }
    名之                                        { tok MingZhi }
    有                                          { tok You }
    除                                          { tok Chu }
    昔之                                        { tok XiZhi }
    今                                          { tok Jin }
    是矣                                        { tok ShiYi }
    今不復存矣                                  { tok JinBuFuCunYi }
    乃得                                        { tok NaiDe }
    乃歸空無                                    { tok NaiGuiKongWu }
    乃得矣                                      { tok NaiDeYi }
    吾嘗觀                                      { tok WuDangGuan }
    之書                                        { tok ZhiShu }
    方悟                                        { tok FangWu }
    之義                                        { tok ZhiYi }
    物                                          { tok Wu }
    其物如是                                    { tok QiWuRuShi }
    物之                                        { tok WuZhi }
    之物也                                      { tok ZhiWuYe }
{

data AlexUserState = AlexUserState
  {
  }

alexInitUserState :: AlexUserState
alexInitUserState = AlexUserState

alexEOF :: Alex RangedToken
alexEOF = do
  (pos, _, _, _) <- alexGetInput
  pure $ RangedToken EOF (Range pos pos)

data Range = Range
  { start :: AlexPosn
  , stop :: AlexPosn
  } deriving (Eq, Show)

data RangedToken = RangedToken
  { rtToken :: Token
  , rtRange :: Range
  } deriving (Eq, Show)

data Token
  -- Identifiers
  = Identifier String
  -- Constants
  | String String
  | HanziInt Integer String
  | HanziFloat Double String
  | Bool Bool
  -- Types
  | WYNum | WYList | WYString | WYBool
  -- Arithmetic binary operations
  | Plus
  | Minus
  | Multiply
  -- Post mod operator
  | PostMod
  -- Logical binary operations
  | IsTrue
  | IsFalse
  | Unary
  -- Comparison operators
  | Eq
  | Neq
  | Lt
  | Le
  | Gt
  | Ge
  -- Preposition
  | PrepLeft
  | PrepRight
  -- If/Else related
  | If
  | Else
  -- For loops related
  | ForStartList
  | ForStartEnum
  | ForStartWhile
  | ForMidList
  | ForMidEnum
  | ForIfEnd
  -- Miscellaneous
  | Print
  | Comment String
  | Flush
  | Break
  -- EOF
  | EOF
  -- A list of all other Chinese keyword that would appear no where other than
  -- an esoteric ancient Chinese programming language like this!
  | Fu
  | Zhi
  | QiYu
  | Chang
  | Xian
  | Qi
  | Chong
  | Shi
  | Qu
  | YiShi
  | WuYou
  | Shu
  | YuXingShiShu
  | BiXianDe
  | Yue
  | ShiShuYue
  | NaiHangShiShuYue
  | ShiWei
  | ZhiShuYe
  | Zhe
  | JinYou
  | MingZhi
  | You
  | Chu
  | XiZhi
  | Jin
  | ShiYi
  | JinBuFuCunYi
  | NaiDe
  | NaiGuiKongWu
  | NaiDeYi
  | WuDangGuan
  | ZhiShu
  | FangWu
  | ZhiYi
  | Wu
  | QiWuRuShi
  | WuZhi
  | ZhiWuYe
  deriving (Eq, Show)

mkRange :: AlexInput -> Int -> Range
mkRange (start, _, _, str) len = Range{start = start, stop = stop}
  where
    stop = foldl' alexMove start $ take len str

tokBool :: AlexAction RangedToken
tokBool inp@(_, _, _, str) len =
  pure RangedToken
    { rtToken = Bool $ hanziToBool $ take len str
    , rtRange = mkRange inp len
    }

tokComment inp@(_, _, _, str) len =
  pure RangedToken
    { rtToken = Comment $ removeFirstTwoAndLastTwo $ drop 2 $ take len str
    , rtRange = mkRange inp len
    }

tokId :: AlexAction RangedToken
tokId inp@(_, _, _, str) len =
  pure RangedToken
    { rtToken = Identifier $ removeFirstAndLast $ take len str
    , rtRange = mkRange inp len
    }

tokString :: AlexAction RangedToken
tokString inp@(_, _, _, str) len =
  pure RangedToken
    { rtToken = String $ removeFirstTwoAndLastTwo $ take len str
    , rtRange = mkRange inp len
    }

tokHanziInt :: AlexAction RangedToken
tokHanziInt inp@(_, _, _, str) len =
  pure RangedToken
    { rtToken = HanziInt (hanziToInt raw) raw
    , rtRange = mkRange inp len
    }
  where raw = take len str

tokHanziFloat :: AlexAction RangedToken
tokHanziFloat inp@(_, _, _, str) len =
  pure RangedToken
    { rtToken = HanziFloat (fromInteger $ hanziToInt $ take idx raw) raw
    , rtRange = mkRange inp len
    }
  where 
    raw = take len str
    idx = fromJust $ elemIndex '又' raw

tok :: Token -> AlexAction RangedToken
tok ctor inp len =
  pure RangedToken
    { rtToken = ctor
    , rtRange = mkRange inp len
    }

scanMany :: String -> Either String [RangedToken]
scanMany input = runAlex input go
  where
    go = do
      output <- alexMonadScan
      if rtToken output == EOF
        then pure [output]
        else (output :) <$> go

}

-- vim: tabstop=2 shiftwidth=2 expandtab
