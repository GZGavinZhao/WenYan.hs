module WenYan.Utils where

digitToInt :: Char -> Integer
digitToInt '零' = 0
digitToInt '一' = 1
digitToInt '二' = 2
digitToInt '三' = 3
digitToInt '四' = 4
digitToInt '五' = 5
digitToInt '六' = 6
digitToInt '七' = 7
digitToInt '八' = 8
digitToInt '九' = 9
digitToInt c = error $ "Unrecognized digit 『『" ++ [c] ++ "』』"

levelToInt :: Char -> Integer
levelToInt '十' = 10
levelToInt '百' = 100
levelToInt '千' = 1000
levelToInt '萬' = 10000
levelToInt '億' = 100000000
levelToInt c = error $ "Unrecognized level 『『" ++ [c] ++ "』』"

isZero :: Char -> Bool
isZero '零' = True
isZero _ = False

isDigit :: Char -> Bool
isDigit '零' = True
isDigit '一' = True
isDigit '二' = True
isDigit '三' = True
isDigit '四' = True
isDigit '五' = True
isDigit '六' = True
isDigit '七' = True
isDigit '八' = True
isDigit '九' = True
isDigit _ = False

isLevel :: Char -> Bool
isLevel '十' = True
isLevel '百' = True
isLevel '千' = True
isLevel _ = False

isSeparator :: Char -> Bool
isSeparator '萬' = True
isSeparator '億' = True
isSeparator _ = False

hanziToInt :: String -> Integer
hanziToInt s = parse $ reverse s
  where
    parse [] = 0
    parse [x]
      | isLevel x || isSeparator x = levelToInt x
      | otherwise = digitToInt x
    parse [x, '十'] = 10 + digitToInt x
    parse (x : xs@(y : ys))
      | isSeparator x = levelToInt x * parse xs
      | isLevel x = digitToInt y * levelToInt x + parse ys
      | isDigit x = digitToInt x + parse xs
      | otherwise = 17

hanziToBool :: String -> Bool
hanziToBool "陽" = True
hanziToBool "陰" = False
hanziToBool s = error $ "『『" ++ s ++ "』』非爻也！"

removeFirstAndLast :: String -> String
removeFirstAndLast [] = [] -- Handle empty string case
removeFirstAndLast [_] = [] -- Handle single character string case
removeFirstAndLast xs = tail (init xs) -- Remove first and last characters

removeFirstTwoAndLastTwo :: String -> String
removeFirstTwoAndLastTwo xs
  | length xs < 4 = [] -- Handle strings with length less than 4
  | otherwise = drop 2 (take (length xs - 2) xs) -- Remove first two and last two characters

fstOf4 :: (a, b, c, d) -> a
fstOf4 (x, _, _, _) = x

evenOf4 :: (a, b, c, d) -> (b, d)
evenOf4 (_, x, _, y) = (x, y)

lstOf4 :: (a, b, c, d) -> d
lstOf4 (_, _, _, x) = x
