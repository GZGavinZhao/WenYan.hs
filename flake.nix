# SPDX-FileCopyrightText: 2021 Serokell <https://serokell.io/>
#
# SPDX-License-Identifier: CC0-1.0

{
  description = "A WenYan compiler written in Haskell.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    llvm-hs-src = {
      url = "github:llvm-hs/llvm-hs/llvm-12";
      flake = false;
    };
    llvm-hs-patch1 = {
      type = "file";
      url = "https://github.com/llvm-hs/llvm-hs/pull/423.patch?narHash=sha256-8OuoYp4x9Qil2F5BbhNRm3YmLv1xdu5G5wWl/bEyd9M=";
      # narHash = "sha256-8OuoYp4x9Qil2F5BbhNRm3YmLv1xdu5G5wWl/bEyd9M=";
      flake = false;
    };
    llvm-hs-patch2 = {
      type = "file";
      url = "https://github.com/llvm-hs/llvm-hs/pull/424.patch?narHash=sha256-xwbeka2gMHKMVblVZt0CExeNqXEwLycw5fCnsvizWx4=";
      # narHash = "sha256-xwbeka2gMHKMVblVZt0CExeNqXEwLycw5fCnsvizWx4=";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, llvm-hs-src, llvm-hs-patch1, llvm-hs-patch2 }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        haskellPackages = pkgs.haskell.packages.ghc92;

        jailbreakUnbreak = pkg:
          pkgs.haskell.lib.doJailbreak (pkg.overrideAttrs (_: { meta = { }; }));

        callCabal2nix = haskellPackages.callCabal2nix;

        llvm-hs-src-patched = pkgs.applyPatches {
          name = "llvm-hs-src-patched";
          src = llvm-hs-src;
          patches = [
            llvm-hs-patch1
            llvm-hs-patch2
          ];
        };

        llvm-hs-pure = callCabal2nix "llvm-hs-pure" "${llvm-hs-src-patched}/llvm-hs-pure" {};
        llvm-hs = (callCabal2nix "llvm-hs" "${llvm-hs-src-patched}/llvm-hs" {
          inherit llvm-hs-pure;
        }).overrideAttrs (oldAttrs: rec {
          buildInputs = oldAttrs.buildInputs ++ [
            pkgs.libxml2
            pkgs.llvm_12
          ];
        });

        packageName = "WenYan" ;
      in {
        packages.${packageName} =
          haskellPackages.callCabal2nix packageName self rec {
            # Dependency overrides go here
            inherit llvm-hs-pure;
            inherit llvm-hs;
          };

        packages.default = self.packages.${system}.${packageName};
        defaultPackage = self.packages.${system}.default;

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            haskellPackages.haskell-language-server # you must build it with your ghc to work
            ghcid
            cabal-install
            hpack
          ];
          inputsFrom = map (__getAttr "env") (__attrValues self.packages.${system});
          env = {
            "LANG" = "C.UTF-8";
          };
        };
        devShell = self.devShells.${system}.default;
      });
}
